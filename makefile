export PATH := $(PATH):$(shell pwd)/drivers/chrome/:$(shell pwd)/drivers/firefox/
#PATH := $(shell pwd):$(PATH)
init:
	pip install -r requirements.txt

prepare:
	chmod +x ./drivers/firefox/geckodriver
	chmod +x ./drivers/chrome/chromedriver
	@echo $$PATH
	python -m pytest main/jira/gui/tests/test_login.py

test:
	python -m pytest main/jira/gui/tests/test_login.py