""" Utils to manage request body"""
import json
import configparser


class FileUtil:
    """Class defined to manage files (Read)"""

    @staticmethod
    def read_json_template(template, base_path='main/jira/api/resources/'):
        """Look if a template can be loaded, if not, return empty dictionary
        :param template: key word for the template to use
        :type template: string
        :param base_path: path of the resources folder
        :type base_path: string
        """
        path = f'{base_path}{template}.json'
        try:
            return json.load(open(path))
        except FileNotFoundError:
            return {}

    @staticmethod
    def read_ini_file(path, section, *args):
        """
        Return ini values according section and arguments
        :param path: pathname of file
        :type path: string
        :param section: section to retrieve
        :type section: string
        :param args: values inside section to retrieve
        :type args: string
        """
        config = configparser.ConfigParser()
        response = {}
        config.read(f'{path}.ini')
        for value in args:
            try:
                response[value] = config[section][value]
            except KeyError:
                raise Exception(f"{value} is not present in ini file")
        return response
