"""Module to manage loggers"""
import logging
import logging.config


def get_logger(logger):
    """ Method to create logger with predefined configuration from logging.conf

    :param logger: name of logger from logging.conf
    :type logger: string
    """
    logging.config.fileConfig('logging.conf')
    return logging.getLogger(logger)
