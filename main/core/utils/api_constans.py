"""Enum class for HTTP methods"""
from enum import Enum


class HTTPMethods(Enum):
    """Can Retrieve get, post, put, delete methods"""
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
