import pytest

from selenium.webdriver import Chrome, Firefox

from main.core.utils.file_utils import FileUtil as filer


@pytest.fixture(scope='session')
def config():
    data = filer.read_ini_file('config', 'browser', 'BROWSER')
    if 'WAIT_TIME' not in data.keys():
        data['WAIT_TIME'] = 10
    print(data['WAIT_TIME'])
    return data


@pytest.fixture
def browser(config):
    if config['BROWSER'] == 'chrome':
        driver = Chrome()
    elif config['BROWSER'] == 'firefox':
        driver = Firefox()
    else:
        raise Exception(f"{config['BROWSER']} is not a supported browser")
    driver.implicitly_wait(config['WAIT_TIME'])
    yield driver
    driver.quit()
