from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class AtlassianLogin:
    USERNAME = (By.ID, "username")
    PASSWORD = (By.ID, "password")
    USERPIC = (By.CSS_SELECTOR, "[role='img']")

    def __init__(self, browser):
        self.browser = browser

    def load(self, url):
        self.browser.get(url)

    def login(self, username, password):
        WebDriverWait(self.browser, 30).until(expected_conditions.visibility_of_element_located(self.USERNAME))
        username_input = self.browser.find_element(*self.USERNAME)
        username_input.send_keys(username + Keys.RETURN)
        WebDriverWait(self.browser, 10).until(expected_conditions.visibility_of_element_located(self.PASSWORD))
        password_input = self.browser.find_element(*self.PASSWORD)
        password_input.send_keys(password + Keys.RETURN)
        WebDriverWait(self.browser, 30).until(expected_conditions.visibility_of_element_located(self.USERPIC))
