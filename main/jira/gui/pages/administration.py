from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

class AtlassianAdministration:
    TITLE = (By.XPATH, "//span[contains(.,'administration')]")

    def __init__(self, browser):
        self.browser = browser

    def title(self):
        WebDriverWait(self.browser, 25).until(expected_conditions.visibility_of_element_located(self.TITLE))
        return self.browser.find_element(*self.TITLE).text